<?php
/**
* Implementation of theme_menu_item().
*
* Add active class to current menu item links.
*/
function phptemplate_menu_item($mid, $children = '', $leaf = TRUE) {
  $item = menu_get_item($mid); // get current menu item

  // decide whether to add the active class to this menu item
  if ((drupal_get_normal_path($item['path']) == $_GET['q']) // if menu item path...
  || (drupal_is_front_page() && $item['path'] == '<front>')) { // or front page...
    $active_class = 'active'; // set active class
  } else { // otherwise...
    $active_class = ''; // do nothing
  }

  return '<li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) . $active_class .'">'. menu_item_link($mid) . $children ."</li>\n";
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/*
  phptemplate_breadcrumb($breadcrumb) modification
  File: template.php
  Use: Hide breadcrumb trails with only 1 crumb, regardless of crumb name ("home" is)
    a popular single-crumb that people like to have removed in some templates.
  Through: PHPTemplate function override
  Benefits: Does not involve extra code in separate view. 
*/
function phptemplate_breadcrumb($breadcrumb) {
  $home = variable_get('site_name', 'drupal');
  $sep = ' &raquo; ';
  // Check if breadcrumb has more than 1 element.
  // Options: Change to the number of elements/crumbs a breadcrumb needs to be visible.
  if (count($breadcrumb) > 1) {
    $breadcrumb[0] = l(t($home), '');
    /*
      Optional: Include page title in breadcrumb.
    
      drupal_get_title() !== ''
        Check if title blank, if that is the case, we cannot include trailing page name.
      strstr(end($breadcrumb),drupal_get_title()) == FALSE
        Some modules will make it so path or breadcrumb will involve duplication of 
        title and node name (such as in the Events module) to remove this, simply 
        take out  && strstr(end($breadcrumb),drupal_get_title()) == FALSE 
      
      Use: Simply uncomment the if structure below (3 lines).
      Special Use: If you wish to use this regardless of elements/crumbs in a breadcrumb
        simply cut/paste the statements inside the "if (count($breadcrumb) > 1)" outside
        of the structure, and delete the extranneous structure. 
    */    
      if ( (drupal_get_title() !== '') && (strstr(end($breadcrumb),drupal_get_title()) ) == FALSE) {
        $breadcrumb[] = t(drupal_get_title(), '');
      }
    
    return implode($sep, $breadcrumb);
  } else {
    // Would only show a single element/crumb (or none), so return nothing.
    // You can remove this statement.
  }
}

function phptemplate_boxClass($vars) {
  if ($vars['sidebar_left'] && $vars['sidebar_right']) {
    $class = "boxBoth";
  } elseif ($vars['sidebar_left']) {
    $class = "boxLeft";
  } elseif ($vars['sidebar_right']) {
    $class = "boxRight";
  } else {
    $class = "box";
  }
  return $class;
}

function _phptemplate_variables($hook, $vars) {
  switch ($hook) {
    case 'page':
      if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == '') {
        $vars['content_is_node'] = TRUE;
      }
    break;
  }
  $vars['boxClass'] = phptemplate_boxClass($vars);
  
  return $vars;
}
?>
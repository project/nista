<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body id="bd">

<div id="container" class="clear-block">
  <div id="header" class="clear-block">
    <a href="<?=$base_path?>" title="<?php print t('Home') ?>">
    <?php 
    if ($logo) {?>
    <img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" />
    <?php } elseif ($site_name) {?>
    <h1 id="site-name">
    <?php print $site_name; ?>
    </h1>
    <?php } ?>
    </a>
  </div>
  <div id="nav">
    <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
  </div>
  <div id="topShadow"></div>
  
  <?php if($breadcrumb) { ?>
  <div id="breadcrumb">
    <div class="wrapper">
      <?php print $breadcrumb; ?>
    </div>
  </div>
  <?php } ?>
  
  <div id="<?php print $boxClass ?>">

    <div id="center" class="column">

      <h1 id="title"><?php print $title ?></h1>

      <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content ?>
      </div>
      <?php if ($sidebar_left) { ?>
      <div id="left" class="column">
        <?php print $sidebar_left ?>
      </div>
      <?php } ?>    
      <?php if ($sidebar_right) { ?>
      <div id="right" class="column">
        <?php print $sidebar_right ?>
      </div>
      <?php } ?>
      <br class="clearfloat" />
    </div>
    <br class="clearfloat" />

  <div id="footer">
    <div class="wrapper">
    <?php
    if (isset($secondary_links)) { 
    print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist'));
    }
    if (isset($footer_message) || isset ($feed_icons)) {
    echo "<p>";
    if(isset($footer_message)) echo "{$footer_message}";
    if(isset($feed_icons)) echo "{$feed_icons}";
    echo "</p>";
    } 
    ?>
    </div>
  </div>
</div>
<?php print $closure ?>
</body>
</html>
